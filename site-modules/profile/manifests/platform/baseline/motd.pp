class profile::platform::baseline::motd {
 $motd = @("MOTD"/L)
    ===========================================================

          Welcome to ${::hostname}


    ===========================================================

    The operating system is: ${::operatingsystem}
            The domain is: ${::domain}

    | MOTD


  # Check if we have a hiera override for the MOTD, otherwise use the default
  $message = lookup('motd', String, 'first', $motd)

  class { '::motd':
    content => $message,
  }

  if !defined(File['/etc/issue']){

    file { '/etc/issue':
      ensure  => file,
      content => $message,
    }

  }

}
